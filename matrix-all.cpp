#include <cassert>
#include <cstddef>
#include <iostream>
#include <stdexcept>
#include <vector>

// this is a "forward declaration" (see sysprog course for more info) needed to
// declare the Multipliable class
template <typename Scalar> struct Matrix;

// part of the expression-templated multiply described later on below
template <typename Scalar> struct Multipliable {
  size_t rows = 0, cols = 0; // a size_t is an unsigned integer large enough for
                             // all sizes (i.e. 32 or 64 bits)
  Multipliable(size_t rows, size_t cols) : rows(rows), cols(cols) {}
  virtual Matrix<Scalar> evaluate() const = 0;
};

template <typename Scalar> struct Matrix : Multipliable<Scalar> {
  std::vector<Scalar> data;  // an 'std::vector' is a growable/shrinkable array

  // the ":" is special syntax for just copying a constructor argument into a
  // member or invoking a superclass constructor or invoking a different
  // constructor of the Matrix class
  Matrix(size_t rows, size_t cols) : Multipliable<Scalar>(rows, cols) {
    data.resize(rows * cols);
  }

  Matrix(Matrix<Scalar> const &other)
      : Multipliable<Scalar>(other.rows, other.cols), data(other.data) {}

  // element access
  Scalar &operator()(size_t r, size_t c) { return data[r * this->cols + c]; }
  Scalar operator()(size_t r, size_t c) const {
    return data[r * this->cols + c];
  }

  // helper object for comma initialization syntax
  struct InserterProxy {
    size_t idx;
    Matrix &mat;

    Matrix &finished() {
      if (idx != mat.rows * mat.cols) {
        throw std::invalid_argument("Not enough or too many elements provided");
      }
      return mat;
    }

    InserterProxy &operator,(Scalar const &val) {
      mat.data[idx++] = val;
      return *this;
    }
    ~InserterProxy() { finished(); }
  };

  // begin comma initialization
  InserterProxy operator<<(Scalar const &val) {
    data[0] = val;
    return {1, *this};
  }

  // "Move-Assignment"
  Matrix &operator=(Matrix const &&other) {
    if (this->rows != other.rows || this->cols != other.cols) {
      throw std::invalid_argument("sizes mismatch");
    }
    // perform a "movement" (for pros: std::move is not necesssary, but makes it
    // more explicit). A movement reassigns ownership of some data to a
    // different owner. In this case, instead of copying other.data into
    // this.data, we "steal" the other's data, drop ours, and leave the other
    // matrix empty. If other is a temporary matrix, this can sometimes be much
    // faster than copying.
    data = std::move(other.data);
  }

  // Expression-Templated Multiplication: Instead of performing operations
  // directly, we create yet another proxy object which represents a "multiply"
  // instruction. This allows us to somewhat intelligently (ab)use associativity
  // to increase performance for the user.
  struct MultiplyResult : Multipliable<Scalar> {
    Multipliable<Scalar> const &left, &right;

    // constructor
    MultiplyResult(Multipliable<Scalar> const &left,
                   Multipliable<Scalar> const &right)
        : Multipliable<Scalar>(left.rows, right.cols), left(left),
          right(right) {}

    // helper to determine cost of a multiply
    size_t cost(Multipliable<Scalar> const &left,
                Multipliable<Scalar> const &right) {
      assert(left.cols == right.rows);
      return right.cols * left.rows * left.cols;
    }

    // decide on associativity based on a simple cost model for the matrix
    // multiplication. This algorithm is not optimal but covers some common
    // cases like A * B * v.
    MultiplyResult operator*(Multipliable<Scalar> const &rhs) {
      if (cost(left, right) + cost(*this, rhs) <
          cost(right, rhs) +
              cost(left, Multipliable<Scalar>(right.rows, rhs.cols))) {
        // faster to do (left * right) * rhs
        return MultiplyResult{*this, rhs};
      } else {
        // faster to do left * (right * rhs)
        return MultiplyResult{left, MultiplyResult{right, rhs}};
      }
    }

    // very poorly performing matrix multiply
    Matrix<Scalar> evaluate() const {
      Matrix<Scalar> const l = left.evaluate();
      Matrix<Scalar> const r = right.evaluate();

      Matrix<Scalar> result(l.rows, r.cols);

      for (size_t k = 0; k < r.cols; ++k) {
        for (size_t s = 0; s < l.rows; ++s) {
          result(s, k) = 0;
          for (size_t i = 0; i < l.cols; ++i) {
            result(s, k) += l(s, i) * r(i, k);
          }
        }
      }

      return result;
    }
  };

  Matrix<Scalar> evaluate() const { return *this; }

  MultiplyResult operator*(Multipliable<Scalar> const &other) {
    return MultiplyResult(*this, other);
  }
};

// "toString": write a string representing the matrix into a C++ "output stream"
template <typename Scalar>
std::ostream &operator<<(std::ostream &stream, Matrix<Scalar> const &mat) {
  for (size_t r = 0; r < mat.rows; ++r) {
    for (size_t c = 0; c < mat.cols; ++c) {
      stream << mat(r, c);
      if (c < mat.cols - 1)
        stream << ' ';
    }
    if (r < mat.rows - 1)
      stream << '\n';
  }
  stream.flush();
  return stream;
}

// lets work our magic!
int main() {
  Matrix<double> A(3, 3);
  A << 1, 2, 3, //
      4, 5, 6,  //
      7, 8, 9;

  std::cout << A << '\n' << (A * A).evaluate() << std::endl;
}
