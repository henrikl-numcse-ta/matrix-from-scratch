#include <cassert>
#include <iostream>
#include <optional>
#include <tuple>
#include <vector>

template <typename Scalar> struct Matrix;

template <typename Scalar> struct DenseBase {
  size_t rows = 0, cols = 0; // a size_t is an unsigned integer large enough for
                             // all sizes (i.e. 32 or 64 bits)
  DenseBase(size_t rows, size_t cols) : rows(rows), cols(cols) {}
  virtual Matrix<Scalar> evaluate() const = 0;
};

template <typename Scalar> struct Matrix : DenseBase<Scalar> {
  std::vector<Scalar> data; // an 'std::vector' is a growable/shrinkable array

  // constructor; initializes the data array.
  Matrix(size_t rows, size_t cols) : DenseBase<Scalar>(rows, cols) {
    data.resize(rows * cols);
  }

  Matrix(Matrix const &other)
      : DenseBase<Scalar>(other.rows, other.cols), data(other.data) {}
  Matrix(Matrix const &&other)
      : DenseBase<Scalar>(other.rows, other.cols), data(std::move(other.data)) {
  }
  Matrix(DenseBase<Scalar> const &base) : Matrix(base.evaluate()) {}

  // column-major element access
  Scalar operator()(size_t r, size_t c) const {
    return data[r * this->cols + c];
  }
  Scalar &operator()(size_t r, size_t c) { return data[r * this->cols + c]; }

  Matrix<Scalar> evaluate() const { return *this; }

  // matrix multiplication using expression templating
  struct MultiplyResult : DenseBase<Scalar> {
    MultiplyResult const *child; // for memory management, this is a hack
    DenseBase<Scalar> const &left, &right;

    MultiplyResult(DenseBase<Scalar> const &left,
                   DenseBase<Scalar> const &right)
        : DenseBase<Scalar>(left.rows, right.cols), child(nullptr), left(left),
          right(right) {}
    MultiplyResult(DenseBase<Scalar> const &left, MultiplyResult const *right)
        : DenseBase<Scalar>(left.rows, right->cols), child(right), left(left),
          right(*child) {}
    ~MultiplyResult() { delete child; }

    // helper to determine cost of a multiply
    size_t cost(DenseBase<Scalar> const &left,
                DenseBase<Scalar> const &right) const {
      assert(left.cols == right.rows);
      return right.cols * left.rows * left.cols;
    }

    struct Dimensions : DenseBase<Scalar> {
      Dimensions(size_t rows, size_t cols) : DenseBase<Scalar>(rows, cols) {}
      Matrix<Scalar> evaluate() const { assert(false); }
    };

    // decide on associativity based on a simple cost model for the matrix
    // multiplication. This algorithm is not optimal but covers some common
    // cases like A * B * v.
    MultiplyResult operator*(DenseBase<Scalar> const &rhs) const {

      if (cost(left, right) + cost(*this, rhs) <
          cost(right, rhs) + cost(left, Dimensions(right.rows, rhs.cols))) {
        // faster to do (left * right) * rhs
        return MultiplyResult(*this, rhs);
      } else {
        // faster to do left * (right * rhs)
        return MultiplyResult(left, new MultiplyResult{right, rhs});
      }
    }

    // very poorly performing matrix multiply
    Matrix<Scalar> evaluate() const {
      Matrix<Scalar> const l = left.evaluate();
      Matrix<Scalar> const r = right.evaluate();

      Matrix<Scalar> result(l.rows, r.cols);

      for (size_t k = 0; k < r.cols; ++k) {
        for (size_t s = 0; s < l.rows; ++s) {
          result(s, k) = 0;
          for (size_t i = 0; i < l.cols; ++i) {
            result(s, k) += l(s, i) * r(i, k);
          }
        }
      }

      return result;
    }
  };

  MultiplyResult operator*(DenseBase<Scalar> const &other) {
    return MultiplyResult(*this, other);
  }

  // helper object for comma initialization syntax
  struct InserterProxy {
    size_t idx;
    Matrix &mat;

    Matrix &finished() {
      if (idx != mat.rows * mat.cols) {
        throw std::invalid_argument("Not enough or too many elements provided");
      }
      return mat;
    }

    InserterProxy &operator,(Scalar const &val) {
      mat.data[idx++] = val;
      return *this;
    }
    ~InserterProxy() { finished(); }
  };

  // begin comma initialization
  InserterProxy operator<<(Scalar const &val) {
    data[0] = val;
    return {1, *this};
  }
};

// "toString": write a string representing the matrix into a C++ "output stream"
template <typename Scalar>
std::ostream &operator<<(std::ostream &stream, Matrix<Scalar> const &mat) {
  for (size_t r = 0; r < mat.rows; ++r) {
    for (size_t c = 0; c < mat.cols; ++c) {
      stream << mat(r, c);
      if (c < mat.cols - 1)
        stream << ' ';
    }
    if (r < mat.rows - 1)
      stream << '\n';
  }
  stream.flush();
  return stream;
}


int main() {
  Matrix<int> A(2, 2);
  A << 1, 2, //
      3, 4;

  Matrix<int> B(2, 2);
  B << 0, 1, //
      1, 0;

  Matrix<int> v(2, 1);
  v << 1, 2;

  Matrix<int> b = A * B * v;

  std::cout << b << std::endl;
}
